from app import api
from resources.bookstore import BookResource


api.add_route('/ebook/', BookResource())