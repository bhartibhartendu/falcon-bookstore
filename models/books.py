from mongoengine import *
import datetime

class BooksModel(Document):
    book_name = StringField(required=True, max_length=100)
    auther_fn = StringField(required=True, max_length=100)
    auther_ln = StringField(required=True, max_length=100)
    published_on = DateTimeField(required=True)
    create_on = DateTimeField(default=datetime.datetime.utcnow)

    def to_dict(self):
    	return {
    		'book_name': self.book_name,
    		'auther_name': self.auther_fn + " " + self.auther_ln,
    		'published_on': self.published_on.isoformat()
    	}

