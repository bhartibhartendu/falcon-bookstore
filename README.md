# falcon-bookstore

This is a sample project to understand the Python-falcon

# Falcon
- This is python based rest framework for building rest api's microservice.
- It is mainly used to build microservice based set of rest API's
- It is very fast as compare to others frameworks like flask and django.
- Falcon https://falconframework.org/
- Benchmark
    - https://medium.com/@gurayy/falcon-a-python-framework-for-writing-excellent-microservices-and-apis-fa2354630c5b


# Project specification
- Python 3
- Mongodb
- Falcon

# Project setup
- clone the repo
- `pip install - r reqirements.txt`
- On linux `gunicorn route:api`
- On windows `waitress-serve --port=8000 route:api`


# TODO
- Add some more API's
- Add auth middleware