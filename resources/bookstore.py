import falcon
import json
import datetime
from models.books import BooksModel


class BookResource(object):

    def on_get(self, req, resp):
        rows = []
        resp.body = json.dumps([u.to_dict() for u in BooksModel.objects.all()]
        )
        resp.status = falcon.HTTP_200


    def on_post(self, req, resp):
        row = BooksModel(
            book_name = req.get_json('book_name'),
            auther_fn = req.get_json('auther_fn'),
            auther_ln = req.get_json('auther_ln'),
            published_on = req.get_json('published_on'),
        )
        row.save()

        resp.json = {'id': str(row.id)}
        resp.status = falcon.HTTP_201